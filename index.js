// Module needed for general functionality
var request = require("request");
var https = require("https");

// Modules needed for Amazon Payments
var qs = require("query-string");
var xmlParser = require('xml2js').parseString;
var tabParser = require('csv-parse');
var crypto = require("crypto");

// Database module for storing into the database
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: process.env.dbhost,
    user: process.env.user,
    password: process.env.password,
    port: process.env.port
});

// External payment processor modules
var paypal = require('paypal-rest-sdk');
var stripe = require('stripe')(process.env.stripe_test_key);

// Module needed for emailing information
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill(process.env.mandrill_key);

// --------------- AWS handler export -----------------------

exports.handler = function(event, context) {
    try {
        console.log("event.session.application.applicationId=" + event.session.application.applicationId);

        if (event.session.new) {
            onSessionStarted({
                requestId: event.request.requestId
            }, event.session);
        }
        if (event.request.type === "LaunchRequest") {
            onLaunch(event.request, event.session, function callback(sessionAttributes, speechletResponse) {
                context.succeed(buildResponse(sessionAttributes, speechletResponse));
            });
        } else if (event.request.type === "IntentRequest") {
            onIntent(event.request, event.session, function callback(sessionAttributes, speechletResponse) {
                context.succeed(buildResponse(sessionAttributes, speechletResponse));
            });
        } else if (event.request.type === "SessionEndedRequest") {
            onSessionEnded(event.request, event.session);
            context.succeed();
        }

    } catch (e) {
        context.fail("Exception: " + e);
    }
};

// --------------- Alexa Skill functions -----------------------

/**
 * Called when the session starts.
 */
function onSessionStarted(sessionStartedRequest, session) {
    console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId + ", sessionId=" + session.sessionId);
}
/**
 * Called when the user launches the skill without specifying what they want.
 */
function onLaunch(launchRequest, session, callback) {
    console.log("onLaunch requestId=" + launchRequest.requestId + ", sessionId=" + session.sessionId);
    var sessionAttributes = {};
    var cardTitle = "Welcome to Charity Pay";
    var speechOutput = "Welcome to Charity Pay. Charity Pay makes donations to any 5o1c3 registered charity in the United States. To get started say, create my profile or make a gift. Now what can I help you with?";
    var repromptText = "Welcome to Charity Pay. Charity Pay makes donations to any 5o1c3 registered charity in the United States. To get started say, create my profile or make a gift. Now what can I help you with?";
    var shouldEndSession = false;
    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}
/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId + ", sessionId=" + session.sessionId);
}
/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session, callback) {
    console.log(session);
    var intent = intentRequest.intent;
    var intentName = intentRequest.intent.name;
    if ("create" === intentName) {
        initiateProfile(intent, session, callback);
    } else if ("make" === intentName) {
        var choice = null;
        howMuchToDonate(intent, session, choice, callback);
    } else if ("change" === intentName) {
        if (session.attributes.chooseOrg) {
            var amount = parseFloat(intent.slots.Dollar.value + "." + intent.slots.Cents.value);
            whichCharityToDonate(intent, session, amount, callback)
        } else {
            changeDonationAmount(intent, session, callback);
        }
    } else if ("give" === intentName) {
        if (session.attributes.chooseOrg) {
            var amount = parseFloat(intent.slots.Dollar.value + "." + intent.slots.Cents.value);
            whichCharityToDonate(intent, session, amount, callback)
        } else {
            var amount = parseFloat(intent.slots.Dollar.value + "." + intent.slots.Cents.value);
            if(amount != '' || amount != undefined) {
                session.attributes.amount = amount;
            }
            existingUserCharge(intent, session, callback);                
        }
    } else if ("donate" === intentName || "don'teight" === intentName || "donteight" === intentName) {
        existingUserCharge(intent, session, callback);
    } else if ("choose" === intentName) {
        var choice = intent.slots.Choice.value;
        howMuchToDonate(intent, session, choice, callback);
    } else if ("AMAZON.YesIntent" === intentName) {
        var attributes = session.attributes;
        authorizeStripeCharge(attributes, callback);
    } else if ("AMAZON.NoIntent" === intentName) {
        var attributes = session.attributes;
        getChangeRequest(attributes, callback)
    } else if ("AMAZON.HelpIntent" === intentName) {
        getHelpResponse(callback);
    } else if ("AMAZON.StopIntent" === intentName) {
        getEndRequest(callback);
    } else if ("AMAZON.CancelIntent" === intentName) {
        getEndRequest(callback);
    } else {
        throw "Invalid intent";
    }
}

// --------------- Help, Change, and End Functions -----------------------

function getHelpResponse(callback) {
    var sessionAttributes = {};
    var cardTitle = "Help";
    var speechOutput = "To start using this skill, say create my profile or make a donation. Now what can I help you with?";
    var repromptText = "To start using this skill, say create my profile or make a donation. Now what can I help you with?";
    var shouldEndSession = false;
    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}

function getChangeRequest(attributes, callback) {
    var cardTitle = "Donation Process Pending";
    var speechOutput = "To stop or cancel your donation say stop or cancel. To change your donation amount, say change amount.";
    var shouldEndSession = false;
    callback(attributes, buildSpeechletResponse(cardTitle, speechOutput, null, shouldEndSession));
}

function getEndRequest(callback) {
    var cardTitle = "Donation Process Stopped";
    var speechOutput = "Your donation has been cancelled.";
    var shouldEndSession = true;
    callback({}, buildSpeechletResponse(cardTitle, speechOutput, null, shouldEndSession));
}

// --------------- Amazon Initiate Function -----------------------

function initiateProfile(intent, session, callback) {
    // Login with Amazon
    var secret_key = process.env.secret_key;
    var userId = stringHash(session.user.userId, secret_key);
    var accessToken = session.user.accessToken;

    // Check if session has access token
    if (accessToken == undefined) {
        var sessionAttributes = {};
        var cardTitle = "Authenticate Your Account";
        var speechOutput = "To start using this skill, please use the companion app to authenticate on Amazon";
        var repromptText = "Please use the companion app on your device to authenticate on Amazon";
        var shouldEndSession = false;
        callback(sessionAttributes, linkAccountResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    } else {
        // need to check if token is still valid...
        var options = {
            url: 'https://api.amazon.com/user/profile?access_token=' + accessToken,
        };
        request.get(options, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                if (body != '') {
                    var user = JSON.parse(body);
                    console.log(user);
                    var user_email = user.email;
                    var user_postal_code = user.postal_code;
                    var user_fullname = user.name;
                    var fullName = user_fullname.split(" ");
                    var user_first_name = fullName[0];
                    var user_last_name = fullName[fullName.length - 1];
                    var user = {
                        "email": user_email,
                        "name": user_first_name,
                        "postalcode": user_postal_code,
                        "fullname": user_fullname
                    }
                    // You have access token in session, now checking the database for user
                    connection.query("SELECT * FROM Alexa_VoiceDonation.user_list WHERE amazon_user_id=" + connection.escape(userId), function(error, results, fields) {
                        if (error) {
                            throw error;
                        } else {
                            // Check if new user who is not in the database
                            console.log(results);
                            if (results == [] || results == undefined || results == '' || !results) {
                                // New user using the skill
                                connection.query('INSERT INTO Alexa_VoiceDonation.user_list (amazon_user_id, amazon_access_token, amazon_email, amazon_first_name, amazon_last_name, amazon_postal_code, amazon_user_fullname ) VALUES (' + connection.escape(userId) + ', ' + connection.escape(accessToken) + ', ' + connection.escape(user_email) + ', ' + connection.escape(user_first_name) + ', ' + connection.escape(user_last_name) + ', ' + connection.escape(user_postal_code) + ', ' + connection.escape(user_fullname) + ') ON DUPLICATE KEY UPDATE amazon_user_id=' + connection.escape(userId), function(error, results, fields) {
                                    if (error) {
                                        throw error;
                                    } else {
                                        console.log("New user stored into the Alexa_VoiceDonation Database");
                                        newUserConfigurationEmail(userId, user, callback);
                                    }
                                });
                            } else if (accessToken != results[0].amazon_access_token) {
                                var amazon_user_id = results[0].amazon_user_id;
                                var nonprofit_ein = results[0].nonprofit_ein; // this is now a pipe list
                                // Update user's token with the current token in db using the skill
                                connection.query('UPDATE Alexa_VoiceDonation.user_list SET amazon_access_token=' + connection.escape(accessToken) + ' WHERE amazon_user_id=' + connection.escape(amazon_user_id), function(error, results, fields) {
                                    if (error) {
                                        throw error;
                                    } else {
                                        console.log("Existing user updated in the Alexa_VoiceDonation Database!");
                                        connection.query('SELECT * FROM Alexa_VoiceDonation.user_list WHERE nonprofit_ein=' + connection.escape(nonprofit_ein), function(error, results, fields) {
                                            if (error) {
                                                throw error;
                                            } else {
                                                if (results == [] || results == undefined || results == '' || !results || nonprofit_ein == [] || nonprofit_ein == '' || nonprofit_ein == undefined || !nonprofit_ein) {
                                                    console.log("Non profit could not be located!");
                                                    addEinConfigurationEmail(userId, user, callback);
                                                } else {
                                                    var organizations = results[0].nonprofit_ein; // now a pipe list
                                                    var einCheck = (organizations.split("|"))[0];
                                                    if (einCheck != '' || einCheck == undefined || !einCheck) {
                                                        whatDoYouWantToDo(userId, accessToken, user, organizations, callback);
                                                    } else {
                                                        connection.query('SELECT * FROM Alexa_VoiceDonation.nonprofit_list WHERE ein=' + connection.escape(organizations), function(error, results, fields) {
                                                            if (error) {
                                                                throw error;
                                                            } else {
                                                                // Non profit details...
                                                                var organizations = {
                                                                    "ein": results[0].ein,
                                                                    "name": results[0].name,
                                                                    "street1": results[0].street1,
                                                                    "street2": results[0].street2,
                                                                    "city": results[0].city,
                                                                    "state": results[0].state,
                                                                    "phone": results[0].phone,
                                                                    "postal_code": results[0].postal_code
                                                                }
                                                                whatDoYouWantToDo(userId, accessToken, user, organizations.ein, callback);
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        });
                                    }
                                });
                            } else {
                                var amazon_user_id = results[0].amazon_user_id;
                                var nonprofit_ein = results[0].nonprofit_ein; //now a pipe list
                                console.log("Access token was the same and now going through charge logic!");
                                connection.query('SELECT * FROM Alexa_VoiceDonation.user_list WHERE nonprofit_ein=' + connection.escape(nonprofit_ein), function(error, results, fields) {
                                    if (error) {
                                        throw error;
                                    } else {
                                        if (results == [] || results == undefined || results == '' || !results) {
                                            console.log("Non profit could not be located!");
                                            addEinConfigurationEmail(userId, user, callback);
                                        } else {
                                            var organizations = results[0].nonprofit_ein; // now a pipe list
                                            var einCheck = (organizations.split("|"))[0];
                                            if (einCheck != '') {
                                                whatDoYouWantToDo(userId, accessToken, user, organizations, callback);
                                            } else {
                                                connection.query('SELECT * FROM Alexa_VoiceDonation.nonprofit_list WHERE ein=' + connection.escape(organizations), function(error, results, fields) {
                                                    if (error) {
                                                        throw error;
                                                    } else {
                                                        // Non profit details...
                                                        var organizations = {
                                                            "ein": results[0].ein,
                                                            "name": results[0].name,
                                                            "street1": results[0].street1,
                                                            "street2": results[0].street2,
                                                            "city": results[0].city,
                                                            "state": results[0].state,
                                                            "phone": results[0].phone,
                                                            "postal_code": results[0].postal_code
                                                        }
                                                        whatDoYouWantToDo(userId, accessToken, user, organizations.ein, callback);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
}

// --------------- Gateway Logic Function -----------------------

function whatDoYouWantToDo(userId, accessToken, user, organizations, callback) {
    var sessionAttributes = {
        "accessToken": accessToken,
        "user": user,
        "ein": organizations
    }
    var eins = (organizations.split("|"))[1];
    if (eins == '' || eins == undefined) {
        sessionAttributes.mult = false;
    } else {
        sessionAttributes.mult = true;
    }
    var cardTitle = "Your Profile Is Ready";
    var speechOutput = "Hi " + user.name + " your profile has already been created. To make a donation, say make a donation.";
    var repromptText = "Hi " + user.name + " your profile has already been created. To make a donation, say make a donation.";
    var shouldEndSession = false;
    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}

// --------------- Existing User Functions -----------------------

function howMuchToDonate(intent, session, choice, callback) {
    var secret_key = "dobest";
    var userId = stringHash(session.user.userId, secret_key);
    connection.query('SELECT * FROM Alexa_VoiceDonation.user_list WHERE amazon_user_id=' + connection.escape(userId), function(error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results == [] || results == undefined || results == '' || !results) {
                console.log("User Not Located!");
                var options = {
                    url: 'https://api.amazon.com/user/profile?access_token=' + session.user.accessToken,
                };
                request.get(options, function(error, response, body) {
                    if (error) {
                        console.log(error);
                    } else {
                        if (body != '') {
                            var user = JSON.parse(body);
                            var user_email = user.email;
                            var user_postal_code = user.postal_code;
                            var user_fullname = user.name;
                            var fullName = user_fullname.split(" ");
                            var user_first_name = fullName[0];
                            var user_last_name = fullName[fullName.length - 1];
                            var user = {
                                "email": user_email,
                                "name": user_first_name,
                                "postalcode": user_postal_code,
                                "fullname": user_fullname
                            }
                            connection.query('INSERT INTO Alexa_VoiceDonation.user_list (amazon_user_id, amazon_access_token, amazon_email, amazon_first_name, amazon_last_name, amazon_postal_code, amazon_user_fullname ) VALUES (' + connection.escape(userId) + ', ' + connection.escape(session.user.accessToken) + ', ' + connection.escape(user_email) + ', ' + connection.escape(user_first_name) + ', ' + connection.escape(user_last_name) + ', ' + connection.escape(user_postal_code) + ', ' + connection.escape(user_fullname) + ') ON DUPLICATE KEY UPDATE amazon_user_id=' + connection.escape(userId), function(error, results, fields) {
                                if (error) {
                                    throw error;
                                } else {
                                    console.log("New user stored into the Alexa_VoiceDonation Database");
                                    newUserConfigurationEmail(userId, user, callback);
                                }
                            });
                        }
                    }
                });
            } else {
                var ein = results[0].nonprofit_ein;
                var einArray = ein.split("|");
                var einCheck = einArray[1];
                var user = {
                    "name": results[0].amazon_first_name,
                    "email": results[0].amazon_email
                }
                if (ein == '' || ein == undefined || ein == [] || !ein) {
                    console.log("User needs to pick charities");
                    newUserConfigurationEmail(userId, user, callback);
                } else if (einCheck == undefined || einCheck == '' || !einCheck || einCheck == []) {
                    console.log("User has single charity");
                    connection.query('SELECT * FROM Alexa_VoiceDonation.nonprofit_list WHERE ein=' + connection.escape(ein), function(error, results, fields) {
                        if (error) {
                            throw error;
                        } else {
                            if (results == [] || results == undefined || results == '' || !results) {
                                newUserConfigurationEmail(userId, user, callback);
                            } else {
                                // Non profit details...
                                var organization = {
                                    "ein": ein,
                                    "name": results[0].name,
                                    "street1": results[0].street1,
                                    "street2": results[0].street2,
                                    "city": results[0].city,
                                    "state": results[0].state,
                                    "phone": results[0].phone,
                                    "postal_code": results[0].postal_code
                                }
                                var sessionAttributes = {
                                    "organization": organization,
                                    "user": user
                                };
                                var cardTitle = "How Much Would You Like To Donate?";
                                var speechOutput = "Hello " + user.name + ", to make a donation to " + organization.name + ", say, give, followed by an amount.";
                                var repromptText = "Hello " + user.name + ", to make a donation to " + organization.name + ", say, give, followed by an amount.";
                                var shouldEndSession = false;
                                callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                            }
                        }
                    });
                } else {
                    console.log("User has multiple charities");
                    if (choice != null) {
                        console.log(choice);
                        var einSplit = einArray[(parseInt(choice) - 1)].split("+");
                        var ein = einSplit[0];
                        console.log(einSplit[1]);
                        connection.query('SELECT * FROM Alexa_VoiceDonation.nonprofit_list WHERE ein=' + connection.escape(ein), function(error, results, fields) {
                            if (error) {
                                throw error;
                            } else {
                                if (results == [] || results == undefined || results == '' || !results) {
                                    newUserConfigurationEmail(userId, user, callback);
                                } else {
                                    // Non profit details...
                                    var organization = {
                                        "ein": ein,
                                        "name": results[0].name,
                                        "street1": results[0].street1,
                                        "street2": results[0].street2,
                                        "city": results[0].city,
                                        "state": results[0].state,
                                        "phone": results[0].phone,
                                        "postal_code": results[0].postal_code
                                    }
                                    session.organization = organization;
                                    existingUserCharge(intent, session, callback);
                                }
                            }
                        });
                    } else {
                        var user = {
                            "name": results[0].amazon_first_name,
                            "email": results[0].amazon_email
                        }
                        var sessionAttributes = {
                            "ein": ein,
                            "user": user,
                            "chooseOrg": true
                        };
                        var cardTitle = "How Much Would You Like To Donate?";
                        var speechOutput = "Hello " + user.name + ", to make a donation say, give, followed by an amount.";
                        var repromptText = "Hello " + user.name + ", to make a donation say, give, followed by an amount.";
                        var shouldEndSession = false;
                        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                    }
                }
            }
        }
    });
}

function whichCharityToDonate(intent, session, amount, callback) {
    var einArray = (session.attributes.ein).split("|");
    var sessionAttributes = {
        "ein": einArray,
        "amount": amount
    }
    var cardTitle = "Please choose your organization";
    var speakChoice = '';
    for (var i = 0; i < einArray.length; i++) {
        var einNameSplit = (einArray[i]).split("+");
        var ein = einNameSplit[0];
        var name = einNameSplit[1];
        speakChoice += "To donate to "+name+" say "+(i + 1);
    }
    var speechOutput = speakChoice;
    var repromptText = "Say choose followed by the number of the charity in your profile.";
    var shouldEndSession = false;
    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}

function changeDonationAmount(intent, session, callback) {
    var secret_key = process.env.secret_key;
    var userId = stringHash(session.user.userId, secret_key);
    connection.query('SELECT * FROM Alexa_VoiceDonation.user_list WHERE amazon_user_id=' + connection.escape(userId), function(error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results == [] || results == undefined || results == '' || !results) {
                console.log("User Not Located!");
                var options = {
                    url: 'https://api.amazon.com/user/profile?access_token=' + session.user.accessToken,
                };
                request.get(options, function(error, response, body) {
                    if (error) {
                        console.log(error);
                    } else {
                        if (body != '') {
                            var userjson = JSON.parse(body);
                            var user = {
                                "name": userjson.name,
                                "email": userjson.email
                            }
                            newUserConfigurationEmail(userId, user, callback);
                        }
                    }
                });
            } else {
                var user = {
                    "name": results[0].amazon_first_name,
                    "email": results[0].amazon_email
                }
                var ein = results[0].nonprofit_ein;
                connection.query('SELECT * FROM Alexa_VoiceDonation.nonprofit_list WHERE ein=' + connection.escape(ein), function(error, results, fields) {
                    if (error) {
                        throw error;
                    } else {
                        // Non profit details...
                        var sessionAttributes = {
                            "organization": organization,
                            "user": user
                        };
                        var einCheck = (ein.split("|"))[1];
                        if(einCheck == undefined || einCheck == '' || einCheck == [] || !einCheck) {
                            sessionAttributes.chooseOrg = false;
                            var organization = {
                                "ein": ein,
                                "name": results[0].name,
                                "street1": results[0].street1,
                                "street2": results[0].street2,
                                "city": results[0].city,
                                "state": results[0].state,
                                "phone": results[0].phone,
                                "postal_code": results[0].postal_code
                            }
                        } else {
                            sessionAttributes.chooseOrg = true;
                            sessionAttributes.ein = ein;
                        }
                        var cardTitle = "Change Donation Amount";
                        var speechOutput = "To change your donation amount, say, give, followed by an amount.";
                        var repromptText = "To change your donation amount, say, give, followed by an amount.";
                        var shouldEndSession = false;
                        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                    }
                });
            }
        }
    });
}

function existingUserCharge(intent, session, callback) {
    var secret_key = process.env.secret_key;
    var userId = stringHash(session.user.userId, secret_key);
    if (session.attributes.amount != '') {
        var amount = session.attributes.amount;
    } else {
        var amount = parseFloat(intent.slots.Dollar.value + "." + intent.slots.Cents.value);
    }
    connection.query('SELECT * FROM Alexa_VoiceDonation.user_list WHERE amazon_user_id=' + connection.escape(userId), function(error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results == [] || results == undefined || results == '' || !results) {
                console.log("User Not Located!");
                var options = {
                    url: 'https://api.amazon.com/user/profile?access_token=' + session.user.accessToken,
                };
                request.get(options, function(error, response, body) {
                    if (error) {
                        console.log(error);
                    } else {
                        if (body != '') {
                            var user = JSON.parse(body);
                            var user_email = user.email;
                            var user_postal_code = user.postal_code;
                            var user_fullname = user.name;
                            var fullName = user_fullname.split(" ");
                            var user_first_name = fullName[0];
                            var user_last_name = fullName[fullName.length - 1];
                            var user = {
                                "email": user_email,
                                "name": user_first_name,
                                "postalcode": user_postal_code,
                                "fullname": user_fullname
                            }
                            connection.query('INSERT INTO Alexa_VoiceDonation.user_list (amazon_user_id, amazon_access_token, amazon_email, amazon_first_name, amazon_last_name, amazon_postal_code, amazon_user_fullname ) VALUES (' + connection.escape(userId) + ', ' + connection.escape(session.user.accessToken) + ', ' + connection.escape(user_email) + ', ' + connection.escape(user_first_name) + ', ' + connection.escape(user_last_name) + ', ' + connection.escape(user_postal_code) + ', ' + connection.escape(user_fullname) + ') ON DUPLICATE KEY UPDATE amazon_user_id=' + connection.escape(userId), function(error, results, fields) {
                                if (error) {
                                    throw error;
                                } else {
                                    console.log("New user stored into the Alexa_VoiceDonation Database");
                                    newUserConfigurationEmail(userId, user, callback);
                                }
                            });
                        }
                    }
                });
            } else {
                var ein = results[0].nonprofit_ein;
                var customerId = results[0].customer_id;
                var email = results[0].amazon_email;
                var name = results[0].amazon_first_name;
                var fullname = results[0].amazon_fullname;
                var postal_code = results[0].amazon_postal_code;
                var user = {
                    "email": results[0].amazon_email,
                    "name": results[0].amazon_first_name
                }
                if (!customerId) {
                    // completed some of the flow, but did not fill credit card or charities
                    console.log("Customer ID not found!");
                    newUserConfigurationEmail(userId, user, callback);
                } else {
                    connection.query('SELECT * FROM Alexa_VoiceDonation.nonprofit_list WHERE ein=' + connection.escape(ein), function(error, results, fields) {
                        if (error) {
                            throw error;
                        } else {
                            if (results == [] || results == undefined || results == '' || !results) {
                                console.log("User's favorite Non-Profit could not be located!");
                                addEinConfigurationEmail(userId, user, callback);
                            } else {
                                // Non profit details...
                                if (session.organization == [] || session.organization == undefined || session.organization == '' || !session.organization) {
                                    var organization = {
                                        "ein": ein,
                                        "name": results[0].name,
                                        "street1": results[0].street1,
                                        "street2": results[0].street2,
                                        "city": results[0].city,
                                        "state": results[0].state,
                                        "phone": results[0].phone,
                                        "postal_code": results[0].postal_code
                                    }
                                } else {
                                    var organization = {
                                        "ein": session.organization.ein,
                                        "name": session.organization.name,
                                        "street1": session.organization.street1,
                                        "street2": session.organization.street2,
                                        "city": session.organization.city,
                                        "state": session.organization.state,
                                        "phone": session.organization.phone,
                                        "postal_code": session.organization.postal_code
                                    }
                                }
                                var sessionAttributes = {
                                    "userId": userId,
                                    "customerId": customerId,
                                    "email": email,
                                    "name": name,
                                    "amount": amount,
                                    "organization": organization,
                                    "state": "stripeinitialize"
                                };
                                if (isNaN(amount)) {
                                    var cardTitle = "I did not understand the donation amount";
                                    var speechOutput = "I did not catch the donation amount, please say give, followed by an amount.";
                                    var repromptText = "I did not catch the donation amount, please say give, followed by an amount.";
                                    var shouldEndSession = false;
                                    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                                } else {
                                    var cardTitle = "Confirm Your Donation";
                                    var speechOutput = "Please confirm your " + amount + " dollar donation to " + organization.name + ". To change this donation say change, to stop this donation say stop, to proceed, say yes.";
                                    var repromptText = "Please confirm your " + amount + " dollar donation to " + organization.name + ". To change this donation say change, to stop this donation say stop, to proceed, say yes.";
                                    var shouldEndSession = false;
                                    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}

// --------------- Stripe Charge Function ------------------------

function authorizeStripeCharge(attributes, callback) {
    console.log(attributes);
    stripe.charges.create({
        amount: ((attributes.amount) * 100),
        currency: 'usd',
        customer: attributes.customerId,
        metadata: {
            'organization_ein': attributes.organization.ein,
            'organization_name': attributes.organization.name,
            'organization_street1': attributes.organization.street1,
            'organization_street2': attributes.organization.street2,
            'organization_city': attributes.organization.city,
            'organization_state': attributes.organization.state,
            'organization_phone': attributes.organization.phone,
            'organization.postal_code': attributes.organization.postal_code
        }
    }, function(err, charge) {
        if (err) {
            console.log("Charge failed! Please try again or with another payment method!");
        } else {
            console.log("This charge was successful!");
            stripeChargeEmail(attributes.email, attributes.name, attributes.amount, attributes.organization.name, callback);
            var cardTitle = "Please wait while we process your gift.";
            var speechOutput = "Please wait while we process your gift";
            var repromptText = "Please wait while we process your gift";
            var shouldEndSession = true;
            buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)
        }
    });
}

// --------------- Emails ------------------------

function newUserConfigurationEmail(userId, user, callback) {
    console.log(user.email);
    var message = {
        "html": "<a href='https://dogoodapp.cdrfg.com?user_id=" + encodeURIComponent(userId) + "'>Complete Your Profile</a>",
        "text": "",
        "subject": "Complete Your Profile",
        "from_email": "developers@cdrfg.com",
        "from_name": "Do Good",
        "to": [{
            "email": user.email,
            "name": user.name,
            "type": "to"
        }],
        "headers": {
            "Reply-To": "developers@cdrfg.com"
        },
        "important": false,
        "track_opens": null,
        "track_clicks": null,
        "auto_text": null,
        "auto_html": null,
        "inline_css": null,
        "url_strip_qs": null,
        "preserve_recipients": null,
        "view_content_link": null,
        "bcc_address": "",
        "tracking_domain": null,
        "signing_domain": null,
        "return_path_domain": null,
        "merge": true,
        "merge_language": "mailchimp",
        "global_merge_vars": [{
            "name": "merge1",
            "content": "merge1 content"
        }],
        "merge_vars": [{
            "rcpt": "aalkondon@cdrfg.com",
            "vars": [{
                "name": "merge2",
                "content": "merge2 content"
            }]
        }],
        "tags": [
            "password-resets"
        ]
    };
    var async = true;
    var ip_pool = "Main Pool";
    var send_at = "";
    mandrill_client.messages.send({
        "message": message,
        "async": async,
        "ip_pool": ip_pool,
        "send_at": send_at
    }, function(result) {
        console.log(result);
        var sessionAttributes = {};
        var cardTitle = "Create My Profile";
        var speechOutput = "Hi " + user.name + " you have been sent an email to complete your profile. Please click on the link in the email to complete your profile.";
        var repromptText = "Please click on the link in the email to finish your profile.";
        var shouldEndSession = true;
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
}

function addEinConfigurationEmail(userId, user, callback) {
    var message = {
        "html": "<a href='https://dogoodapp.cdrfg.com/charity?user_id=" + encodeURIComponent(userId) + "'>Add Your Charity</a>",
        "text": "",
        "subject": "Add Charity",
        "from_email": "developers@cdrfg.com",
        "from_name": "Do Good",
        "to": [{
            "email": user.email,
            "name": user.name,
            "type": "to"
        }],
        "headers": {
            "Reply-To": "developers@cdrfg.com"
        },
        "important": false,
        "track_opens": null,
        "track_clicks": null,
        "auto_text": null,
        "auto_html": null,
        "inline_css": null,
        "url_strip_qs": null,
        "preserve_recipients": null,
        "view_content_link": null,
        "bcc_address": "",
        "tracking_domain": null,
        "signing_domain": null,
        "return_path_domain": null,
        "merge": true,
        "merge_language": "mailchimp",
        "global_merge_vars": [{
            "name": "merge1",
            "content": "merge1 content"
        }],
        "merge_vars": [{
            "rcpt": "aalkondon@cdrfg.com",
            "vars": [{
                "name": "merge2",
                "content": "merge2 content"
            }]
        }],
        "tags": [
            "password-resets"
        ]
    };
    var async = true;
    var ip_pool = "Main Pool";
    var send_at = "";
    mandrill_client.messages.send({
        "message": message,
        "async": async,
        "ip_pool": ip_pool,
        "send_at": send_at
    }, function(result) {
        console.log(result);
        var sessionAttributes = {};
        var cardTitle = "Create My Profile";
        var speechOutput = "Hi " + user.name + " you have been sent an email to complete your profile. Please click on the link in the email to complete your profile.";
        var repromptText = "Please click on the link in the email to complete your profile.";
        var shouldEndSession = true;
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
}

function stripeChargeEmail(email, name, amount, organization, callback) {
    var message = {
        "html": "Hi " + name + " you have been charged $" + amount + "!",
        "text": "",
        "subject": "Your transaction with Do Good",
        "from_email": "developers@cdrfg.com",
        "from_name": "Do Good",
        "to": [{
            "email": email,
            "name": name,
            "type": "to"
        }],
        "headers": {
            "Reply-To": "developers@cdrfg.com"
        },
        "important": false,
        "track_opens": null,
        "track_clicks": null,
        "auto_text": null,
        "auto_html": null,
        "inline_css": null,
        "url_strip_qs": null,
        "preserve_recipients": null,
        "view_content_link": null,
        "bcc_address": "",
        "tracking_domain": null,
        "signing_domain": null,
        "return_path_domain": null,
        "merge": true,
        "merge_language": "mailchimp",
        "global_merge_vars": [{
            "name": "merge1",
            "content": "merge1 content"
        }],
        "merge_vars": [{
            "rcpt": "aalkondon@cdrfg.com",
            "vars": [{
                "name": "merge2",
                "content": "merge2 content"
            }]
        }],
        "tags": [
            "password-resets"
        ]
    };
    var async = false;
    var ip_pool = "Main Pool";
    var send_at = "";
    mandrill_client.messages.send({
        "message": message,
        "async": async,
        "ip_pool": ip_pool,
        "send_at": send_at
    }, function(result) {
        console.log(result);
        var sessionAttributes = {};
        var cardTitle = "Thank for your donation!";
        var speechOutput = "Congratulations! We have successfully processed your " + amount + " dollar donation to " + organization + ". You will receive an email confirmation of your gift shortly. Thank you!";
        var repromptText = "Thank you for your donation.";
        var shouldEndSession = true;
        callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
}

function stringHash(string, secret_key) {
    var hashedstring = crypto.createHmac('sha256', secret_key).update(string).digest('base64');
    return hashedstring;
}

// --------------- Amazon Charge Functions -----------------------
/*
function createOrderReference(intent, accessToken, host, path, secret_key, callback) {
    var requestData = {
        path: path,
        query: {
            AccessToken: accessToken,            
            Action: 'CreateOrderReference',
            MWSAuthToken: 'amzn.mws.f2ab1187-f903-8cb4-9787-7625a5fd5af1',           
            Version: '2013-01-01'
        }
    };
    var stringToSign = formatRequestData(requestData);
    var data = signString(stringToSign, host, secret_key);

    makeCreateOrderRequest(intent, data, host, secret_key, callback);
}

function makeCreateOrderRequest(intent, requestData, host, secret_key, callback) {
    var options = {
        url: 'https://' + host + ':' + '443' + requestData.path,
        headers: {
            Host: host,
        },
        qs: requestData.query
    };
    if (requestData.headers && requestData.headers['User-Agent']) {
        options.headers['User-Agent'] = requestData.headers['User-Agent'];
    } else {
        options.headers['User-Agent'] = 'swift-donate' + '/' + '1.0.0' + ' (Language=JavaScript)';
    }
    if (requestData.headers && requestData.headers['Content-Type']) {
        options.headers['Content-Type'] = requestData.headers['Content-Type'];
    } else if (requestData.feedContent) {
        if (requestData.feedContent.slice(0, 5) === '<?xml') {
          options.headers['Content-Type'] = 'text/xml';
        } else {
          options.headers['Content-Type'] = 'text/tab-separated-values; charset=iso-8859-1';
        }
    } else {
        options.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
    }
    if (requestData.feedContent) {
        options.body = requestData.feedContent;
        options.headers['Content-MD5'] = crypto.createHash('md5').update(requestData.feedContent).digest('base64');
    }
    request.post(options, function(error, response, body) {
        if(error) {
            console.log(error);
        } else {
            if(body != '') {
                console.log(body);
                xmlParser(body, function (err, result) {
                    var stringData = JSON.parse(JSON.stringify(result));
                    console.log(JSON.stringify(result));
                    var amount = parseFloat(intent.slots.Dollar.value + "." + intent.slots.Cents.value);
                    var organization = intent.slots.Organization.value;
                    var oroId = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["AmazonOrderReferenceId"][0];
                    var cardType = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["PaymentDescriptor"][0]["Name"][0];
                    var cardTail = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["PaymentDescriptor"][0]["AccountNumberTail"][0];
                    var donorName = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["Buyer"][0]["Name"][0];
                    var donorEmail = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["Buyer"][0]["Email"][0];
                    var donorState = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["Destination"][0]["PhysicalDestination"][0]["StateOrRegion"][0];
                    var donorPhone = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["Destination"][0]["PhysicalDestination"][0]["Phone"][0];
                    var donorCity = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["Destination"][0]["PhysicalDestination"][0]["City"][0];
                    var donorCountry = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["Destination"][0]["PhysicalDestination"][0]["CountryCode"][0];
                    var donorPostalCode = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["Destination"][0]["PhysicalDestination"][0]["PostalCode"][0];
                    var donorStreet = stringData["CreateOrderReferenceResponse"]["CreateOrderReferenceResult"][0]["OrderReferenceDetails"][0]["Destination"][0]["PhysicalDestination"][0]["AddressLine1"][0];          
                    if(isNaN(amount)) {
                        var sessionAttributes = {
                            amountUndefined:true
                        };
                        var cardTitle = "Amount or Organization undefined";
                        var speechOutput = "I did not understand the amount and / or organization that you said, could you please repeat the phrase?";
                        var repromptText = "Please repeat the phrase, I didn't quite catch it the first time.";
                        var shouldEndSession = true;
                    } else {
                        var sessionAttributes = {
                            amount: amount.toFixed(2),
                            organization: organization,
                            host: host,
                            oroId: oroId.toString(),
                            cardType: cardType,
                            cardTail: cardTail,
                            donorName: donorName,
                            donorEmail: donorEmail,
                            donorState: donorState,
                            donorPhone: donorPhone,
                            donorCity: donorCity,
                            donorCountry: donorCountry,
                            donorPostalCode: donorPostalCode,
                            donorStreet: donorStreet,
                            requestData: requestData,
                            secret_key: secret_key,
                            amountUndefined: false
                        };

                        var firstname = (donorName).split(" ")[0];
                        var cardTitle = "Successfully logged in with Amazon!";
                        var speechOutput = "Hi " + firstname + ",  you have successfully logged in with Amazon and an email has been sent to you with a link to add your credit card and choose your favorite charities to which you would like to donate.";
                        var repromptText = "Please open up your email account linked to your amazon account and fill in your credit card and charity information.";
                        var shouldEndSession = true;
                    }
                    callback(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                });
            } else {
                console.log("Body is empty");
            }
        }
    });
}

function setOrderReference(attributes, callback) {
    var amount = attributes.amount;
    var requestData = {
        path: attributes.requestData.path,
        query: {
            AccessToken: attributes.requestData.query.AccessToken,            
            Action: 'SetOrderReferenceDetails',
            AmazonOrderReferenceId: attributes.oroId,
            MWSAuthToken: 'amzn.mws.f2ab1187-f903-8cb4-9787-7625a5fd5af1',
            'OrderReferenceAttributes.OrderTotal.CurrencyCode': 'USD',
            'OrderReferenceAttributes.OrderTotal.Amount': attributes.amount,        
            Version: '2013-01-01'
        }
    };
    var stringToSign = formatRequestData(requestData);
    var data = signString(stringToSign, attributes.host, attributes.secret_key);

    attributes.requestData = data;
    makeSetOrderRequest(attributes, callback);
}

function makeSetOrderRequest(attributes, callback) {
    var options = {
        url: 'https://' + attributes.host + ':' + '443' + attributes.requestData.path,
        headers: {
            Host: attributes.host,
        },
        qs: attributes.requestData.query
    };
    if (attributes.requestData.headers && attributes.requestData.headers['User-Agent']) {
        options.headers['User-Agent'] = attributes.requestData.headers['User-Agent'];
    } else {
        options.headers['User-Agent'] = 'swift-donate' + '/' + '1.0.0' + ' (Language=JavaScript)';
    }
    if (attributes.requestData.headers && attributes.requestData.headers['Content-Type']) {
        options.headers['Content-Type'] = attributes.requestData.headers['Content-Type'];
    } else if (attributes.requestData.feedContent) {
        if (attributes.requestData.feedContent.slice(0, 5) === '<?xml') {
          options.headers['Content-Type'] = 'text/xml';
        } else {
          options.headers['Content-Type'] = 'text/tab-separated-values; charset=iso-8859-1';
        }
    } else {
        options.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
    }
    if (attributes.requestData.feedContent) {
        options.body = attributes.requestData.feedContent;
        options.headers['Content-MD5'] = crypto.createHash('md5').update(attributes.requestData.feedContent).digest('base64');
    }
    request.post(options, function(error, response, body) {
        if(error) {
            console.log(error);
        } else {
            if(body != '') {
                console.log(body);
                xmlParser(body, function (err, result) {
                    var stringData = JSON.stringify(result);
                    confirmOrderReference(attributes, callback);
                });
            } else {
                console.log("Body is empty");
            }
        }
    });
}

function confirmOrderReference(attributes, callback) {
    var requestData = {
        path: attributes.requestData.path,
        query: {
            AccessToken: attributes.requestData.query.AccessToken,            
            Action: 'ConfirmOrderReference',
            AmazonOrderReferenceId: attributes.oroId,
            MWSAuthToken: 'amzn.mws.f2ab1187-f903-8cb4-9787-7625a5fd5af1',      
            Version: '2013-01-01'
        }
    };
    var stringToSign = formatRequestData(requestData);
    var data = signString(stringToSign, attributes.host, attributes.secret_key);

    attributes.requestData = data;
    makeConfirmOrderRequest(attributes, callback);
}

function makeConfirmOrderRequest(attributes, callback) {
    var options = {
        url: 'https://' + attributes.host + ':' + '443' + attributes.requestData.path,
        headers: {
            Host: attributes.host,
        },
        qs: attributes.requestData.query
    };
    if (attributes.requestData.headers && attributes.requestData.headers['User-Agent']) {
        options.headers['User-Agent'] = attributes.requestData.headers['User-Agent'];
    } else {
        options.headers['User-Agent'] = 'swift-donate' + '/' + '1.0.0' + ' (Language=JavaScript)';
    }
    if (attributes.requestData.headers && attributes.requestData.headers['Content-Type']) {
        options.headers['Content-Type'] = attributes.requestData.headers['Content-Type'];
    } else if (attributes.requestData.feedContent) {
        if (attributes.requestData.feedContent.slice(0, 5) === '<?xml') {
          options.headers['Content-Type'] = 'text/xml';
        } else {
          options.headers['Content-Type'] = 'text/tab-separated-values; charset=iso-8859-1';
        }
    } else {
        options.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
    }
    if (attributes.requestData.feedContent) {
        options.body = attributes.requestData.feedContent;
        options.headers['Content-MD5'] = crypto.createHash('md5').update(attributes.requestData.feedContent).digest('base64');
    }
    request.post(options, function(error, response, body) {
        if(error) {
            console.log(error);
        } else {
            if(body != '') {
                console.log(body);
                xmlParser(body, function (err, result) {
                    var stringData = JSON.stringify(result);
                    attributes.chargeCard = true;
                    var firstname = (attributes.donorName).split(" ")[0];

                    var cardTitle = "Confirm your final donation";
                    var speechOutput = firstname + ", are you sure you would like to donate " + attributes.amount + " dollars to " + attributes.organization + " with your, " + attributes.cardType + " credit card ending in, " + attributes.cardTail + ". Please say yes or no.";
                    var repromptText = "Please confirm whether you would like to donate " + attributes.amount + " to " + attributes.organization + " with you credit card ending in " + attributes.cardTail;
                    var shouldEndSession = false;

                    callback(attributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
                });
            } else {
                console.log("Body is empty");
            }
        }
    });
}

function authorizeCharge(attributes, callback) {
    var requestData = {
        path: attributes.requestData.path,
        query: {
            AccessToken: attributes.requestData.query.AccessToken,            
            Action: 'Authorize',
            AmazonOrderReferenceId: attributes.oroId,
            AuthorizationReferenceId: attributes.oroId,
            'AuthorizationAmount.CurrencyCode': 'USD',
            'AuthorizationAmount.Amount': attributes.amount,
            CaptureNow: 'true',
            MWSAuthToken: 'amzn.mws.f2ab1187-f903-8cb4-9787-7625a5fd5af1',      
            Version: '2013-01-01'
        }
    };
    var stringToSign = formatRequestData(requestData);
    var data = signString(stringToSign, attributes.host, attributes.secret_key);

    attributes.requestData = data;
    makeAuthorizeChargeRequest(attributes, callback);
}

function makeAuthorizeChargeRequest(attributes, callback) {
    var options = {
        url: 'https://' + attributes.host + ':' + '443' + attributes.requestData.path,
        headers: {
            Host: attributes.host,
        },
        qs: attributes.requestData.query
    };
    if (attributes.requestData.headers && attributes.requestData.headers['User-Agent']) {
        options.headers['User-Agent'] = attributes.requestData.headers['User-Agent'];
    } else {
        options.headers['User-Agent'] = 'swift-donate' + '/' + '1.0.0' + ' (Language=JavaScript)';
    }
    if (attributes.requestData.headers && attributes.requestData.headers['Content-Type']) {
        options.headers['Content-Type'] = attributes.requestData.headers['Content-Type'];
    } else if (attributes.requestData.feedContent) {
        if (attributes.requestData.feedContent.slice(0, 5) === '<?xml') {
          options.headers['Content-Type'] = 'text/xml';
        } else {
          options.headers['Content-Type'] = 'text/tab-separated-values; charset=iso-8859-1';
        }
    } else {
        options.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
    }
    if (attributes.requestData.feedContent) {
        options.body = attributes.requestData.feedContent;
        options.headers['Content-MD5'] = crypto.createHash('md5').update(attributes.requestData.feedContent).digest('base64');
    }
    request.post(options, function(error, response, body) {
        if(error) {
            console.log(error);
        } else {
            if(body != '') {
                console.log(body);
                xmlParser(body, function (err, result) {
                    var stringData = JSON.stringify(result);
                    console.log(stringData);
                    sendOfflineDonationRecord(attributes, callback);
                });
            } else {
                console.log("Body is empty");
            }
        }
    });
}
*/

// --------------- Helper Functions -----------------------

function formatRequestData(requestData) {
    if (!requestData.path) {
        requestData.path = '/';
    }
    if (!requestData.query.Timestamp) {
        requestData.query.Timestamp = (new Date()).toISOString();
    }
    if (!requestData.query.AWSAccessKeyId) {
        requestData.query.AWSAccessKeyId = 'AKIAIQ2ERYWRXMMUDFJA';
    }
    if (!requestData.query.SellerId) {
        requestData.query.SellerId = 'AV0ROZ839GLVJ';
    }
    if (!requestData.responseFormat) {
        requestData.responseFormat = 'xml';
    }
    requestData.query.SignatureMethod = 'HmacSHA256';
    requestData.query.SignatureVersion = '2';

    return requestData;
}


function signString(requestData, host, secret_key) {
    var stringToSign = ["POST", host, requestData.path, qs.stringify(requestData.query)].join('\n');
    requestData.query.Signature = crypto.createHmac('sha256', secret_key).update(stringToSign).digest('base64');

    return requestData;
}

// --------------- Alexa Helper Functions -----------------------

function linkAccountResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "SSML",
            ssml: "<speak>"+output+"</speak>"
        },
        card: {
            type: "LinkAccount",
            title: "SessionSpeechlet - " + title,
            content: "SessionSpeechlet - " + output
        },
        reprompt: {
            outputSpeech: {
                type: "SSML",
                ssml: "<speak>"+repromptText+"</speak>"
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "SSML",
            ssml: "<speak>"+output+"</speak>"
        },
        card: {
            type: "Simple",
            title: "SessionSpeechlet - " + title,
            content: "SessionSpeechlet - " + output
        },
        reprompt: {
            outputSpeech: {
                type: "SSML",
                ssml: "<speak>"+repromptText+"</speak>"
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildResponse(sessionAttributes, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttributes,
        response: speechletResponse
    };
}